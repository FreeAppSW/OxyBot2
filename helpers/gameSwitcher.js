const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const possibleGames = require("../cmd_resources/gamesList.json");
const util = require("../utils.js");
var pgame = "";
discord.user.setActivity("Initialization complete.", {type: "PLAYING"});
setInterval(function(){
    let res = util.randomInt(0, possibleGames.length - 1);
    var game;
    /*do*/ game = possibleGames[res].replace("##guilds_count##", discord.guilds.array().length).replace("##pref##", config.prefix).replace("##channels_count##", discord.channels.array().length).replace("##user_count##", discord.users.array().length); /*while (game == pgame);*/
    discord.user.setActivity(game, {type: "PLAYING"});
    pgame = game;
}, 35000);