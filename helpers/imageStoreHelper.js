var msgArray = {};

function getNextURL(guildId){
    msgArray[guildId].i++;
    return msgArray[guildId].links[msgArray[guildId].i];
}
function getPrevURL(guildId){
    msgArray[guildId].i--;
    return msgArray[guildId].links[msgArray[guildId].i];
}
function setupNewGuild(guildId, linksArray){
    msgArray[guildId] = {};
    msgArray[guildId].links = linksArray;
    msgArray[guildId].i = 0;
}

module.exports = {
    create: setupNewGuild,
    getNextURL: getNextURL,
    getPrevURL: getPrevURL,
    object: guildId => msgArray[guildId]
}