var counter = 0;
var i = () => {
    counter++;
}
var cnt = () => {
    return counter;
}
module.exports = {
    cnt: cnt,
    i: i
}