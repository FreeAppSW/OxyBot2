const client = require("./start.js").client;
const config = require("./start.js").config;

var msgIsCommand = (message, name, dmAvailable, channelAvailable) => {
    return (message.channel.type != "dm" && channelAvailable && (message.content.toLowerCase().startsWith(config.prefix+name.toLowerCase()) ||  message.content.toLowerCase().startsWith("<@" + client.user.id + "> " + name.toLowerCase()) || message.content.toLowerCase().startsWith("<@!" + client.user.id + "> " + name.toLowerCase()))) || (message.channel.type == "dm" && message.content.toLowerCase().startsWith(name.toLowerCase()) && dmAvailable) && !message.author.bot;
}

function quotedStrings(arr) {
    let continueExec = true;
    while (continueExec) {
        let foundQuote = false;
        let quotePos = 0;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].startsWith('"')) {
                foundQuote = true;
                quotePos = i;
            }
            if (arr[i].endsWith('"')) {
                if (foundQuote) {
                    let newString = arr[quotePos];
                    let elements = arr.splice(quotePos + 1, i - quotePos);
                    for (var j=0; j< elements.length; j++) {
                        newString = `${newString} ${elements[j]}`;
                    }
                    newString = newString.substring(1);
                    newString = newString.slice(0, -1);
                    arr[quotePos] = newString;
                    break;
                }
            }
            if (i === arr.length - 1) {
                continueExec = false;
            }
        }
    }
    return arr;
}

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

  function rand(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

function generateRandomString(cnt = 13){
    var possible = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "_", "-"];
    var out = "";
    for (var i = 1; i != cnt; i++){
        out+= possible[rand(0, 63)];
    }
    return out;
}
function secondsToHMS(secCnt){
    seconds = secCnt / 1000;
            return `${Math.floor(Math.floor(seconds) / 86400)}d, ${(((Math.floor(seconds) % 86400) / 3600) < 10 ? "0" : "")}${Math.floor((Math.floor(seconds) % 86400) / 3600)}:${(((Math.floor(seconds) % 86400) % 3600)/60 < 10 ? "0" : "")}${Math.floor(((Math.floor(seconds) % 86400) % 3600) / 60)}:${(((Math.floor(seconds) % 86400) % 3600 % 60) < 10 ? "0" : "")}${((Math.floor(seconds) % 86400) % 3600) % 60}`;
}
module.exports = {
    msgIsCommand: msgIsCommand,
    splitString: quotedStrings,
    getKeyByValue: getKeyByValue,
    generateRandomString: generateRandomString,
    secondsToHMS: secondsToHMS,
    randomInt: rand
}