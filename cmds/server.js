const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "server",
    helpVisible: true,
    helpDescription: "server",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"InformationalCommands",
    cmdAvailableInDM: false,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                var channelCount = [0,0];
                message.channel.guild.channels.forEach((element) => {
                    if (element.type == "text") channelCount[0]++;
                    else if (element.type == "voice") channelCount[1]++;
                })
                var channelEmbed = new discordapp.RichEmbed({
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "EmbedTitle"),
                    fields: [
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "GuildNameCatTitle"),
                            value: message.guild.name
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "GuildOwnerCatTitle"),
                            value: message.guild.owner
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "GuildIdCatTitle"),
                            value: message.guild.id
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "MemberCountCatTitle"),
                            value: message.guild.memberCount
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "TextChannelLabelInline"),
                            value: channelCount[0],
                            inline: true
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "VoiceChannelLabelInline"),
                            value: channelCount[1],
                            inline: true
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "GuildCreatedCatTitle"),
                            value: message.guild.createdAt.toString()
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "VoiceRegionCatTitle"),                            
                            value: message.guild.region
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "VerificationLevelCatTitle"),                                                        
                            value: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "serverInfo", "VerificationLevels")[message.guild.verificationLevel]
                        }
                    ]
                }).setThumbnail(message.guild.iconURL);
                message.channel.send("",{embed: channelEmbed.setColor(0x03A9F4)});
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err));
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + " IID: " + IID + "; Module: " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
        
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}