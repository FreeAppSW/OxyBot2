const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "userinfo",
    helpVisible: true,
    helpDescription: "userinfo",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"Misc",
    cmdAvailableInDM: false,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                let usersToDisplay = (args.length != 1 ? message.mentions.members.array() : [message.member]);
                usersToDisplay.forEach(member => {
                    let channelEmbed = new discordapp.RichEmbed({
                        title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "userInfo", "EmbedTitle"),
                        fields: [
                            {
                                name: "DiscordTag",
                                value: "`" + member.user.username + "#" + member.user.discriminator + "`"
                            },
                            {
                                name:  i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "userInfo", "UserIdCatTitle"),
                                value: member.user.id
                            },
                            {
                                name:  i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "userInfo", "RolesCatTitle"),
                                value: member.roles.map(x => x.name).join("\n")                                
                            },
                            {
                                name:  i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "userInfo", "JoinedThisGuildCatTitle"),
                                value: member.joinedAt.toString()
                            },
                            {
                                name:  i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "userInfo", "JoinedDiscordCatTitle"),
                                value: member.user.createdAt.toString()
                            }
                        ]
                    })
                    let badges = " ";
                    if (member.user.bot) badges += "<:BOT:384442792274821131> ";
                    switch(member.user.presence.status){
                        case "online":
                        if ((member.user.presence.game !== null && member.user.presence.game !== undefined)) if (member.user.presence.game.streaming) badges += "<:Streaming:384442792216100867>";
                            else badges += "<:Online:384457832910684163>";
                            break;
                        case "offline":
                            badges += "<:Offline:384442792312700928>";
                            break;
                        case "dnd":
                            badges += "<:DND:384442792711290882>";
                            break;
                        case "idle":
                            badges += "<:Away:384442792555970560>";
                            break;
                    }
                    if (member.user.presence.game !== null && member.user.presence.game !== undefined) channelEmbed.addField(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "userInfo", "CurrentGameCatTitle"), member.user.presence.game.name);
                    channelEmbed.setDescription(member.displayName + badges).setThumbnail(member.user.avatarURL).setColor(0x03A9F4);
                    message.channel.send("",{embed:channelEmbed});
                });
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err))
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + "IID: " + IID + " Module " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
 
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}