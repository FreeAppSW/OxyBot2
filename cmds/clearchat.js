const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "clearchat",
    helpVisible: true,
    helpDescription: "clearchat",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"ModerationCommands",
    cmdAvailableInDM: false,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                if (message.member.permissions.has("MANAGE_MESSAGES")){
                    let ntd = parseInt(args[1]) + 1;
                      if (ntd > 99){
                          var ntda = ntd-1;
                          while (ntd > 99){
                              ntd -= 99;
                              message.channel.bulkDelete(99);
                          }
                          message.channel.bulkDelete(ntd).then(() => {ntd--; message.channel.send(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "clearChat", "SuccessMessage").replace("##ntd##", ntda));}).catch(e => {var channelEmbed = new discordapp.RichEmbed(); channelEmbed.setTitle(":x: " + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id),"clearChat", "ErrorEmbedTitle")).setColor(0xFF0000).setDescription(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "clearChat", "BotCantDeleteError")); message.channel.send("", {embed: channelEmbed});});
                      }
                      else {
                          message.channel.bulkDelete(ntd).then(() => {ntd--; message.channel.send(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "clearChat", "SuccessMessage").replace("##ntd##", ntd));}).catch(e => {var channelEmbed = new discordapp.RichEmbed(); channelEmbed.setTitle(":x: " + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "actions", "ErrorEmbedTitle").setColor(0xFF0000).setDescription(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "clearChat", "BotCantDeleteError"))); message.channel.send("", {embed: channelEmbed});});
                      }
                }
                else{
                    message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "clearChat", "ClientNoPermission"))
                }
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err))
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + " IID: " + IID + " Module " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
 
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}