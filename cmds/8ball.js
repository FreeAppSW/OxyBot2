const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "8ball",
    helpVisible: true,
    helpDescription: "eightBall",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"FunCommands",
    cmdAvailableInDM: true,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                let query = args[1];
                let number = util.randomInt(0, i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "8ball", "Possible").length - 1);
                let channelEmbed = new discordapp.RichEmbed().setTitle(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id),"8ball", "EmbedTitle")).setDescription(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "8ball", "EmbedDesc")).addField(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "8ball", "YourQuestionCatTitle"), query).addField(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "8ball", "AnswerCatTitle"), i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "8ball", "Possible")[number]).setColor(0x03A9F4);
                message.channel.send("", {embed: channelEmbed});
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err))
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + "IID: " + IID + " Module " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
 
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}