const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
const GoogleImages = require("google-images");
const get = require("snekfetch").get;
//const client = new GoogleImages(require("../token.json").googleImages.SEID, require("../token.json").googleImages.APIKey);

//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "image",
    helpVisible: true,
    helpDescription: "image",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"InformationalCommands",
    cmdAvailableInDM: true,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try {
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                var res;
                let query = args[1];
                let escapedQuery = encodeURIComponent(query);
                let result = await get("https://scraper.kaanlikescoding.me/image?text=" + escapedQuery)
                let parsedResponse = JSON.parse(result.text);
                if (parsedResponse.error || parsedResponse.links == undefined) throw "Error: " + parsedResponse.error;
                if (parsedResponse.links.length != 0){
                    let msg = await message.channel.send("**" + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "image", "ImageResultsFor") + "** `"+ query + "`: " + parsedResponse.links[0]);
                    require("../helpers/imageStoreHelper.js").create(msg.id, parsedResponse.links);
                    await msg.react("👈");
                    await msg.react("👉");
                } else {
                    message.channel.send("**" + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id),"image", "ErrorNoResult")+ "** `" + query + "`.");
                }
    
    //    client.search(query).then(function(result){
    //         if (result.length == 0){
    //             message.channel.send("**" + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id),"image", "ErrorNoResult")+ "** `" + query + "`.");
    //         } else { 
    //             res = result[0].url;
    //             message.channel.send("**" + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "image", "ImageResultsFor") + "** `"+ query + "`: " + res);
    //         }
    // }).catch(e => {throw e;});
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err))
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + " IID: " + IID + " Module " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
 
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}