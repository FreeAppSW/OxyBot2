const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
const canvas = require("canvas");
//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "fquote",
    helpVisible: true,
    helpDescription: "fquote",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"FunCommands",
    cmdAvailableInDM: false,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                let mentionedMember = message.mentions.members.first();
                let newCanvas = canvas.createCanvas(500,90);
               let ctx =  newCanvas.getContext('2d');
                let userAvatar = new canvas.Image();
                canvas.registerFont("./Whitney-Book.ttf",{family: "DiFont"})
                userAvatar.src = mentionedMember.user.avatarURL;
                userAvatar.onload = () => {
                    ctx.fillStyle = (mentionedMember.displayHexColor ? mentionedMember.displayHexColor : "white")
                    ctx.font = "22px DiFont";
                    let c = ctx.measureText(mentionedMember.displayName)
                    ctx.fillText(mentionedMember.displayName, 95, 36);
                    ctx.fillStyle = "rgba(255,255,255,0.2)";
                    ctx.font = "18px DiFont";
                    ctx.fillText("Today at 21:30", 95 + c.width + 8, 36);
                    ctx.fillStyle = "white";
                    ctx.font = "21px DiFont";
                    ctx.fillText(args[2], 95, 63);
                    ctx.beginPath();
                    ctx.arc(50, 50, 30, 0, Math.PI * 2, true);
                    ctx.closePath();
                    ctx.clip();
                    ctx.drawImage(userAvatar, 20, 20, 60, 60);
                    ctx.beginPath();
                    ctx.arc(0, 0, 30, 0, Math.PI * 2, true);
                    ctx.clip();
                    ctx.closePath();
                    ctx.restore();
                    let streamOut = newCanvas.createPNGStream();
                    message.channel.sendFile(streamOut, "pngStream.png");
                }
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err))
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + " IID: " + IID + " Module " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
 
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}