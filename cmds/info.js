const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const IID = require("../start.js").iid;
const i18n = require("../i18n.js");
//const database = require("../dbCacheMgr.js");

const commandMetadata = {
    cmdName: "stats",
    helpVisible: true,
    helpDescription: "status",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory:"Misc",
    cmdAvailableInDM: false,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                let channelEmbed = new discordapp.RichEmbed({
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "EmbedTitle"),
                    fields: [
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "NameStats"),
                            value: `**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.GuildCount")}**: ${discord.guilds.array().length}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.ChannelCount")}**: ${discord.channels.array().length}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.UserCount")}**: ${discord.users.array().length}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.RAMUsage")}**: ${Math.round(process.memoryUsage().rss / 1024 / 1024 * 100) / 100} MB\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.UptimeProcess")}**: ${util.secondsToHMS(process.uptime()*1000)}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.UptimeDiscord")}**: ${util.secondsToHMS(discord.uptime)}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Stats.CommandsRan")}**: ${require("../helpers/execCounter.js").cnt()}`                            
                        },
                        {
                            name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "NameInformation"),
                            value: `**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Information.BotVersion")}**: ${require("../package.json").version}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Information.NodeVersion")}**: ${process.version}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Information.DJSVersion")}**: ${require("../package.json").dependencies["discord.js"]}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Information.OBCoreVersion")}**: ${config.version}\n**${i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "botStats", "Information.SimplePlayerVersion")}**: 1.0`
                        }
                    ]
                }).setColor(0x03A9F4);
                message.channel.send("", {embed: channelEmbed});
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err))
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error:" + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + " IID: " + IID + "; Module: " + __filename}
                }).setColor(0xFF0000)
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
 
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}