const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const i18n = require("../i18n.js");
const IID = require("../start.js").iid;
const dbCache = require("../dbCacheMgr.js").objectCache;

const commandMetadata = {
    cmdName: "help",
    helpVisible: true,
    helpDescription: "help",
    accessLevel: "everyone",
    accessList: [],
    cmdCategory: "Misc",
    cmdAvailableInDM: true,
    cmdAvailableInChannels: true
}

var command = async (message) => {
    if (util.msgIsCommand(message, commandMetadata.cmdName, commandMetadata.cmdAvailableInDM, commandMetadata.cmdAvailableInChannels)){
        if (!config.blackList.includes(message.author.id) && (commandMetadata.accessLevel == "everyone" || (commandMetadata.accessLevel = "list" && commandMetadata.accessList.includes(message.author.id)) || config.owners.includes(message.author.id))){
            try{
                let rawArgs = message.content.split(" ");
                if (message.content.startsWith("<@")) rawArgs.splice(0,1);
                let args = util.splitString(rawArgs);
                var commandsArray = [];
                var categoriesArray = [];
                let files = require("fs").readdirSync("./cmds/");
                //Add commands from cmds/*
                files.forEach(elem => {
                    if (elem[0] != "_"){
                        let cmdData = require("./" + elem).metadata;
                        if ((cmdData.helpVisible && (cmdData.accessLevel == "everyone" || (cmdData.accessLevel == "list" && cmdData.accessList.includes(message.author.id)))) || config.owners.includes(message.author.id))commandsArray.push(cmdData);
                        if (!categoriesArray.includes(cmdData.cmdCategory)) categoriesArray.push(cmdData.cmdCategory);
                    }
                });
                //Add commands from guild custom actions
                let dbLocal = dbCache.actions.filter(x => x.guild_id == message.guild.id);
                if (dbLocal){    
                    var strToAdd = "";
                    dbLocal.forEach(cmda => {
                        let cmd = JSON.parse(cmda.args);
                        if (cmd.visibleHelp) strToAdd += "**action " + cmda.name + "**" + (cmd.description ? " - " + cmd.description : "") + "\n";
                    })
                }
                var channelEmbed = new discordapp.RichEmbed().setColor(0x03A9F4).setTitle("Help").setDescription(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "helpUI", "CommandsOfBotEmbedTitle").replace("%%bot_name%%", config.botName));
                categoriesArray.forEach(elem => {
                    channelEmbed.addField(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "helpDescs", "Cat" + elem), commandsArray.filter(x => x.cmdCategory == elem).map(x => (!x.helpVisible ? "`[Invisible]` " : "") + (x.accessLevel == "owner" ? "`[Owner Only]` " : "") + (x.accessLevel == "list" ? "`[Whitelist]` " : "") + (!x.cmdAvailableInChannels && x.cmdAvailableInDM ? "`[DM Only]` " : "") + "**" + x.cmdName + "** - " + i18n.getLocaleString(i18n.getGuildLocale(message.guild.id),"helpDescs", x.helpDescription)).join("\n"))
                });
                if (strToAdd != "") channelEmbed.addField(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "helpDescs", "CatCustomActions"), strToAdd)
                channelEmbed.setFooter("The command prefix is " + config.prefix + " or @" + discord.user.tag)
                try{
                await message.author.send("", {embed: channelEmbed});
                if (message.channel.type != "dm")  message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "helpUI", "CheckDMMessage"))
                } catch(err){
                    if (message.channel.type != "dm")  await message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "helpUI", "DMDisabledError"))
                }
                require("../helpers/execCounter.js").i();
            } catch (err) {
                log.pushToLog("Command Error", "Command " + commandMetadata.cmdName, require("util").inspect(err));
                let channelEmbed = new discordapp.RichEmbed({
                    author: {name: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorGeneric")},
                    title: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreCommandError"),
                    description: "Error: " + err,
                    footer:{text: i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCoreErrorDebugInfo") + " IID: " + IID + "; Module: " + __filename}
                }).setColor(0xFF0000);
                message.reply("", {embed: channelEmbed});
            }
        } else {
            message.reply(i18n.getLocaleString(i18n.getGuildLocale(message.guild.id), "CoreUI", "OBCorePermissionError"))
        }
    }
}

module.exports = {
    command: command,
    metadata: commandMetadata
}