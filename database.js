const sqlAcct = require("./token.json").sqlAcct;
const config = require("./start.js").config;
const mysql = require("mysql");

try {
    var mysqli = mysql.createPool({
        connectionLimit: 5,
        host: config.sql.server,
        user: sqlAcct.user,
        password: sqlAcct.passwd,
        database: config.sql.database
    });
} catch (err) {
    console.log("Warning: database connection failed, expect problems")
}

mysqli.on("error", err => {
    console.log("mysql error occured");
})

module.exports = mysqli;