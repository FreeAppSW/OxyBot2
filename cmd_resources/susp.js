module.exports =  (member) => {
    let userAccountTime = Math.floor(member.user.createdAt.getTime() / 1000 / 60 / 60 / 24)
    let currentTime = Math.floor(new Date().getTime() / 1000 / 60 / 60 / 24);
    return (currentTime - userAccountTime < 1 && member.user.avatarURL == undefined)
}