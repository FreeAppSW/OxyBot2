const expressApp = require("express")();
const dbCacheMgr = require("./dbCacheMgr.js");
const discord = require("./start.js").client;
const log = require("./logging.js").pushToLog;
const chalk = require("chalk");

expressApp.get("/", (req, res) => {
    res.status(200);
    res.send({
        human: "Everything alright",
        iid: require("./start.js").iid,
        state: 200
    })
})
module.exports = expressApp
require("./music/spAPIService.js");
expressApp.listen(8800, () => {
    console.log(chalk.greenBright("HTTP listener is up on") + " http://127.0.0.1:8800");
    log("Information", "httpAPI", "listener started");
});