var VCs = [];
const i18n = require("../i18n.js");
const sf = require("snekfetch");
const discord = require("../start.js").client;
function playTrack(channel, url, initialChannel){
    sf.get("http://127.0.0.1:5030/play?channel=" + channel.id + "&url=" + encodeURIComponent(url.url)).then(() => {
        initialChannel.send("Playing " + (url.title ? url.title : url.url));
    });
}
function handleEndSignal(channelId){
    VCs[channelId].queue.splice(0,1);
    if (VCs[channelId].queue.length == 0){
        VCs[channelId].initialChannel.send(i18n.getLocaleString(i18n.getGuildLocale(discord.channels.get(channelId).guild.id), "music", "MsgQueueEnded"));
    } else {
        playTrack(VCs[channelId].guild.channels.get(channelId), VCs[channelId].queue[0], VCs[channelId].initialChannel);
    }
}
function handleDotPlay(channel, url, initialChannel){
    if (!VCs[channel.id]) VCs[channel.id] = {guild: channel.guild, initialChannel: initialChannel, queue: [], channelID: channel.id}; else VCs[channel.id].initialChannel = initialChannel;
    if (VCs[channel.id].queue.length !== 0) addToQueue(channel, url, initialChannel); else { 
        VCs[channel.id].queue.push({url: url});
        playTrack(channel, {url: url}, initialChannel);
    }
}
function addToQueue(channel, url, initialChannel){
    VCs[channel.id].queue.push({url: url});
    initialChannel.send("Added " + url);
}
function skip(channelId){
    sf.get("http://127.0.0.1:5030/stop?channel=" + channelId).then(() => {
        VCs[channelId].initialChannel.send(i18n.getLocaleString(i18n.getGuildLocale(discord.channels.get(channelId).guild.id), "music", "MsgTrackSkip"))
    });
}
function stop(channelId){
    VCs[channelId].queue = [];
    sf.get("http://127.0.0.1:5030/stop?channel=" + channelId).then(() => {
        VCs[channelId].initialChannel.send(i18n.getLocaleString(i18n.getGuildLocale(discord.channels.get(channelId).guild.id), "music", "MsgTrackStop"))
    });
}

async function getPlayerStatus(channelId){
    return new Promise(async (res, rej) => {
        let response = await sf.get("http://127.0.0.1:5030/status?channel=" + channelId); 
        res(JSON.parse(response.text));
    })
}
async function leave(channelID) {
    await sf.get("http://127.0.0.1:5030/leave?channel=" + channelID);
}
module.exports = {
   play: handleDotPlay,
   handleEndSignal: handleEndSignal,
   vcs: () => {return VCs;},
   stop: stop,
   skip: skip,
   getPlayerStatus: getPlayerStatus,
   leaveChannel: leave
}