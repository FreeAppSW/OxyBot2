// require("../httpAPI.js").get("/simpleplayer_api/ping", (req, res) => {
//     res.status(200);
//     res.send({
//         human: "I am here",
//         state: 200
//     })
// })

require("../httpAPI.js").get("/simpleplayer_api/playbackFinished", (req, res) => {
    let VCobject = Object.values(require("./queueMgr.js").vcs());
    VCobject = VCobject.find(x => x.guild.id == req.query.channel);
    require("./queueMgr.js").handleEndSignal(VCobject.channelID);
    res.status(200);
    res.send({
        state: 200
    })
})