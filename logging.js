const instanceId = require("./start.js").iid;
const fs = require("fs");
var pushToLog = (msgType, msgEmitter, msg) => {
    fs.appendFile("./logs/" + instanceId + ".log", "[" + new Date().toString() + "][" + msgType.toUpperCase() + "] *" + msgEmitter +  "*: " + msg + "\n", () => {} );
}

module.exports = {
    pushToLog: pushToLog
}