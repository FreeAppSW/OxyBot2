const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const database = require("../database.js");

const eventName = "guildDelete";
const handler = async (guild) => {
    log.pushToLog("GUILD LEAVE", "Left guild `" + guild.name + "` owned by " + guild.owner.user.username + "#" + guild.owner.user.discriminator + ". Member count: " + guild.memberCount);
    database.query("DELETE FROM guilds WHERE guild_id = '" + guild.id + "'", () => {});
}

module.exports = {
    name: eventName,
    handler: handler
}