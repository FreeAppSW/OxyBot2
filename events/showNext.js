const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
//const database = require("../database.js");

const eventName = "messageReactionAdd";
const handler = async (reaction, user) => {
    if (require("../helpers/imageStoreHelper.js").object(reaction.message.id) && user.id != discord.user.id){
        if (reaction.emoji.name == "👉"){
            await reaction.message.edit(require("../helpers/imageStoreHelper.js").getNextURL(reaction.message.id));
            await reaction.message.clearReactions();
            await reaction.message.react("👈");
            await reaction.message.react("👉");
        }
        if (reaction.emoji.name == "👈"){
            await reaction.message.edit(require("../helpers/imageStoreHelper.js").getPrevURL(reaction.message.id));
            await reaction.message.clearReactions();
            await reaction.message.react("👈");
            await reaction.message.react("👉");
        }
    }
}

module.exports = {
    name: eventName,
    handler: handler
}