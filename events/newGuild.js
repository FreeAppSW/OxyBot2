const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const database = require("../database.js");

const eventName = "guildCreate";
const handler = async (guild) => {
    log.pushToLog("GUILD JOIN", "Joined guild `" + guild.name + "` owned by " + guild.owner.user.username + "#" + guild.owner.user.discriminator + ". Member count: " + guild.memberCount);
    database.query("INSERT INTO guilds (guild_id, owner, user_whitelist) VALUES ('" + guild.id + "', '" + guild.owner.user.id + "', '[]')");
}

module.exports = {
    name: eventName,
    handler: handler
}