const discord = require("../start.js").client;
const discordapp = require("../start.js").lib;
const config = require("../start.js").config;
const util = require("../utils.js");
const log = require("../logging.js");
const database = require("../dbCacheMgr.js");

const eventName = "guildMemberAdd";
const handler = async (member) => {
    let dbelem = database.objectCache.guild_prefs.find(x => x.guild_id == member.guild.id);
    if (dbelem == undefined || dbelem.prefs == undefined) dbelem = database.objectCache.guild_prefs.find(x => x.guild_id == "_DEFAULT");
    var thisgprefs = JSON.parse(dbelem.prefs);
    if (thisgprefs.welcoming){
        if (thisgprefs.welcomeChannel !== null){
           var sendChannel = member.guild.channels.find(x => x.id == thisgprefs.welcomeChannel);
           var messageToSend;
           if (thisgprefs.welcomeMessage == null){
               messageToSend = i18n.getLocaleString(thisgprefs.locale,"welcomingTemplates","welcomeMessageDef").replace("##mention_user##", "<@" + member.id + ">").replace("##user_name##", member.user.username.replace("##", "#\#")).replace("##guild_name##", member.guild.name.replace("##", "#\#")).replace("##guild_id##", member.guild.id).replace("##user_id##", member.id).replace("##guild_member_count##", member.guild.memberCount); 
           } else {
               messageToSend = thisgprefs.welcomeMessage.replace("##mention_user##", "<@" + member.id + ">").replace("##user_name##", member.user.username.replace("##", "#\#")).replace("##guild_name##", member.guild.name.replace("##", "#\#")).replace("##guild_id##", member.guild.id).replace("##user_id##", member.id).replace("##guild_member_count##", member.guild.memberCount).replace("##user_full_tag##", member.user);
           }
           sendChannel.send(messageToSend);
       }
    }
}

module.exports = {
    name: eventName,
    handler: handler
}