const discordapp = require("discord.js");
const discord = new discordapp.Client();
const config = require("./config.json");
const tokens = require("./token.json");
const fs = require("fs");
const chalk = require("chalk");
function generateRandomString(cnt = 13){
    var possible = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "_", "-"];
    var out = "";
    for (var i = 1; i != cnt; i++){
        out+= possible[rand(0, 63)];
    }
    return out;
}
function rand(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}
//Generate an Instance ID used in various debug purposes
const instanceID = generateRandomString();

module.exports = {
    lib: discordapp,
    client: discord,
    config: config,
    iid: instanceID
}
require("./dbCacheMgr.js"); //Initialize database cache
const log = require("./logging.js");

console.log(chalk.yellow("Starting bot..."));
console.log("IID: " + instanceID);
console.log(chalk.yellow("Loading commands..."))

log.pushToLog("Information", "Startup", "Instance started with ID " + instanceID);

//Load all modules skipping commented ones
discord.setMaxListeners(100);
fs.readdir("./cmds", async (err, files) => {
    files.forEach(x => {
        if (x[0] != "_"){
            try{
                let reqCmd = require("./cmds/" + x);
                discord.on("message", reqCmd.command);
                console.log(reqCmd.metadata.cmdName + " (" + reqCmd.metadata.cmdCategory + ") loaded.");
                log.pushToLog("Information", "Startup", "Module cmds/" + x + "( command [" + reqCmd.metadata.cmdCategory + "]:" + reqCmd.metadata.cmdName + ") successfully loaded.")
            }
            catch (err) {
                console.log(chalk.redBright("Error loading module cmds/" + x));
                log.pushToLog("Error", "Startup", "Error loading module cmds/" + x + ": " + require("util").inspect(err))
            }
        }
    })
    console.log(chalk.yellow("Loading events..."))
    fs.readdir("./events", async (err, files) => {
        files.forEach(x => {
            if (x[0] != "_"){
                try{
                let reqevnt = require("./events/" + x);
                discord.on(reqevnt.name, reqevnt.handler);
                console.log("Module events/" + x +" loaded.");
                log.pushToLog("Information", "Startup", "Module events/" + x + " (event " + reqevnt.name +") successfully loaded.")
                } catch (err){
                    console.log(chalk.redBright("Error loading module events/" + x));
                    log.pushToLog("Error", "Startup", "Error loading module events/" + x + ": " + err)
                }
            }
        })
    })
    
});
require("./httpAPI.js");
let handleReady = () => {
    discord.removeListener("ready", handleReady);
    console.log(chalk.greenBright("Discord connection is ready"));
    log.pushToLog("Information", "Startup", "Connected to discord gateway");
    fs.readdir("./helpers", async (err, files) => {
        console.log(chalk.yellow("Loading helpers..."))
        files.forEach(x => {
            if (x[0] != "_"){
                try{
                require("./helpers/" + x);
                console.log("Module helpers/" + x + " loaded.");
                log.pushToLog("Information", "Startup", "Module helpers/" + x + " successfully loaded.")
                } catch (err){
                    console.log(chalk.redBright("Error loading module helpers/" + x));
                    log.pushToLog("Error", "Startup", "Error loading module helpers/" + x + ": " + err)
                }
            }
        })
    });
}
discord.on("ready", handleReady);
discord.on("ready", () => {
    log.pushToLog("Information", "Runtime", "Reconnected to discord gateway");
})
discord.on("reconnecting", () => {
    log.pushToLog("Information", "Runtime", "Trying to reconnect to discord gateway");
})

discord.on("error", err => {
    log.pushToLog("Error", "Runtime", err.message);
})


discord.login(tokens.discordToken);