const dbHandle = require("./database.js");
var cache = {};
function cacheTable(tblName){
    dbHandle.query("SELECT * FROM `" + tblName +"`", (err, value) => {
        cache[tblName] = value;
    });
}
function pushData(tblName, dataObject){
    let str = "";
    dataObject.forEach(obj => { str += dbHandle.escape(obj) + ","; str.splice(str.length - 1, 1);});
    dbHandle.query("INSERT INTO " + tblName + "('" + Object.keys(dataObject).join("','") + "') VALUES (" + str + ")");
    cache[tblName].push(dataObject);
}
cacheTable("actions");
cacheTable("guild_prefs");

module.exports = {
    cacheTable: cacheTable,
    pushData: pushData,
    objectCache: cache
}