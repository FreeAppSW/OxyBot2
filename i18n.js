const fs = require("fs");
var cacheManager = require("./dbCacheMgr.js");
function getGuildLocale(guildID){
    let section = cacheManager.objectCache.guild_prefs.find(x => x.guild_id == guildID);
    if (!section) section = cacheManager.objectCache.guild_prefs.find(x => x.guild_id == "_DEFAULT");
    return JSON.parse(section.prefs).locale;
}
function getLocaleString(localeID, moduleID, stringID){
    try{
      var module = fs.readFileSync("./i18n/" + localeID + "/" + moduleID + ".json")
    } catch(err) {
         throw new ReferenceError("i18n: Module " + localeID + "/" +moduleID + " not found");
    }
    try{
        var ret = JSON.parse(module)[stringID];
    } catch(err) {
      throw new ReferenceError("i18n: String " + stringID + " not found in module " + localeID + "/" + moduleID);
    }
    return (ret ? ret : false);
}

module.exports = {
  getLocaleString: getLocaleString,
  getGuildLocale: getGuildLocale
}